<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 14.02.2017
 * Time: 12:59
 */
?>
<div class="wrap">

	<p><?php  printf(__('Settings', VKW_PlUGIN_TEXTDOMAIN));?></p>
<form method="post">
    <table class="form-table">
        <tr valign="top">
            <th scope="row"><?php printf(__('Id user or group', VKW_PlUGIN_TEXTDOMAIN));?></th>
            <td>
                <input type="text" name="groupId" class="large-text code" value="<?php echo get_option( 'groupId' ); ?>" placeholder="<?php echo __( 'Paste count here', VKW_PlUGIN_TEXTDOMAIN ); ?>">
            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><?php echo __( 'Paste this shortcode in content zone', VKW_PlUGIN_TEXTDOMAIN ); ?></th>
            <td>
                <code>[vk-wall]</code>
            </td>
        </tr>

    </table>
    <div class="submit">
        <input name="save" type="submit" class="button-primary" value="<?php echo __('Save settings', VKW_PlUGIN_TEXTDOMAIN); ?>" />
    </div>
</form>
</div>

