<?php

/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 14.02.2017
 * Time: 12:21
 */

class functions
{
    public $vkw_name;
    public $url;
    public $data;
    public $api;

    const VKW_TABLE = "vkw_info_table";
    /**
     * Возвращает название таблицы с префиксом WordPress тот что задаеться при установке
     * всем таблицам
     * @return string
     */
    static public function getTableName(){
        global $wpdb;
        return $wpdb->prefix .static::VKW_TABLE;
    }
    function __construct()
{

    $this->vkw_name = __( 'VK wall content', VKW_PlUGIN_TEXTDOMAIN );

//    add_action( 'admin_menu', array( $this, 'ugl_create_menu' ) );

    // Добавляем шорткод на выдачу контента
    add_shortcode( 'vk-wall' , array( $this, 'getinfo' ));
    add_action('wp_enqueue_scripts', array( $this,'my_stylesheet_admin'));
    add_action('init', 'register_post_types');
    add_action('init', 'create_taxonomy');



    }

    /* Activation action */
    static public function activation() {

        global $wpdb;
        $tableName = self::getTableName();
        $sql = "CREATE TABLE " .$tableName. "(
                              id int(11) NOT NULL AUTO_INCREMENT,
                              gr_id text NOT NULL,
                              PRIMARY KEY (id)
                            ) CHARACTER SET utf8 COLLATE utf8_general_ci;";
        // Проверяем на наличие таблицы в базе данных и если ее нет то создаем
        if($wpdb->get_var("show tables like '$tableName'") != $tableName) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
            $wpdb->query( "INSERT INTO `{$wpdb->prefix}vkw_info_table`(`id`) VALUES (2)" );

        }
    }

    /* Uninstall action*/
    static public function uninstall() {
        // debug.log
        error_log('plugin ' . VKW_PlUGIN_NAME . ' uninstall');
    }


//
//    function ugl_setting_page(){
//        include_once VKW_PlUGIN_DIR.'/assets/admin-page.php';
//        // Обработка запроса
//        if (isset( $_POST['save']) ) {
//            $this->saveOptions( $_POST['val'] );
//        }
//
//    }


//    private function saveOptions( $val ) {



//        if( $val>=-1 ) {
//            echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>' . __('Settings saved.', VKW_PlUGIN_TEXTDOMAIN) . '</b></p></div>';
//
//
//            update_option( 'val', stripslashes($val) );
//        } else  echo '<div id="setting-error" class="settings-error"><p><b>' . __('Error! Field filled incorrectly a number of posts!', VKW_PlUGIN_TEXTDOMAIN) . '</b></p></div>';



//    }

public function insert(){
    global $wpdb;
   $wpdb->query( "INSERT INTO `{$wpdb->prefix}vkw_info_table` VALUES  `{$_POST['vkw_option']}` " );
}
    public function getinfo(){
        global $wpdb;
        $val = $wpdb->get_var( "SELECT gr_id FROM `{$wpdb->prefix}vkw_info_table` " );


        $this->vkw_name = __( 'Vk wall', VKW_PlUGIN_TEXTDOMAIN );
        $this->api = 'https://api.vk.com/method/wall.get?owner_id=-';
        $this->url = file_get_contents($this->api.$val);
        $this->data = json_decode($this->url, true);
        set_transient( 'special_query_results', $this->data,10*0*0);
        $special_query_results =  get_transient( 'special_query_results' );
        foreach ( $special_query_results['response'] as $special_query_results ) {


            if( $special_query_results['text'] ) echo '<p class="vk_wall_par">' .$special_query_results['text'] . '</p>';



        }

    }

    function ugl_create_menu() {
        //add_options_page( __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' . $this->vkw_name, __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' .$this->vkw_name, 'edit_themes','vkw-setting', array( $this, 'ugl_setting_page' ) );
//        add_menu_page( __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' . $this->vkw_name, __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' .$this->vkw_name, 'edit_themes','vkw-setting', array( $this, 'ugl_setting_page' ) );
//        add_submenu_page('vkw-setting',  __('Info', VKW_PlUGIN_TEXTDOMAIN) . ' - ' . $this->vkw_name,  __('Info', VKW_PlUGIN_TEXTDOMAIN) . ' - ' . $this->vkw_name, 'edit_themes', 'vkw-info', array( $this, 'ugl_info_page' ) );
    }

    function my_stylesheet_admin(){

        wp_enqueue_style("style-admin",VKW_PlUGIN_URL.'assets/css/vk_wall.css');
    }





}

class VK_Widget extends WP_Widget {

    // Регистрация виджета используя основной класс
    function __construct() {
        // вызов конструктора выглядит так:
        // __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
        parent::__construct(
            '',
            'Последние записи с VK',
            array('description' => 'Виджет выводит последние посты с группы VK')
        );

        // скрипты/стили виджета, только если он активен
        if ( is_active_widget( false, false, $this->id_base ) || is_customize_preview() ) {
            add_action('wp_enqueue_scripts', array( $this, 'add_my_widget_scripts' ));
            add_action('wp_head', array( $this, 'add_my_widget_style' ) );
        }
    }

    /**
     * Вывод виджета во Фронт-энде
     *
     * @param array $args     аргументы виджета.
     * @param array $instance сохраненные данные из настроек
     */
    function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];
        if ( ! empty( $title ) ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo  do_shortcode('[vk-wall]');
        echo $args['after_widget'];

    }

    /**
     * Админ-часть виджета
     *
     * @param array $instance сохраненные данные из настроек
     */
    function form( $instance ) {
        $title = @ $instance['title'] ?: 'Последние посты с группы VK';

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    /**
     * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance новые настройки
     * @param array $old_instance предыдущие настройки
     *
     * @return array данные которые будут сохранены
     */
    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

    // скрипт виджета
    function add_my_widget_scripts() {
        // фильтр чтобы можно было отключить скрипты
        if( ! apply_filters( 'show_my_widget_script', true, $this->id_base ) )
            return;

        $theme_url = get_stylesheet_directory_uri();

        wp_enqueue_script('my_widget_script', $theme_url .'/my_widget_script.js' );
    }

    // стили виджета
    function add_my_widget_style() {
        // фильтр чтобы можно было отключить стили
        if( ! apply_filters( 'show_my_widget_style', true, $this->id_base ) )
            return;
        ?>
        <style type="text/css">
            .my_widget a{ display:inline; }
        </style>
        <?php
    }

}
// конец класса VK_Widget

// регистрация VK_Widget в WordPress
function register_vk_widget() {
    register_widget( 'VK_Widget' );
}
add_action( 'widgets_init', 'register_vk_widget' );


do_action('wp_dashboard_setup');
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );

// Выводит контент
function dashboard_widget_function( $post, $callback_args ) {
    echo "Последние посты с группы ВК";
    echo  do_shortcode('[vk-wall]');
    ?>
    <style>
        .vk_wall_par {
            background: #eeeeee;
            padding: 20px;
        }
    </style>
<?php

}

// Используется в хуке
function add_dashboard_widgets() {
    wp_add_dashboard_widget('dashboard_widget', 'Последние записи в группе VK', 'dashboard_widget_function');
}

function register_post_types(){
    register_post_type('post_coffe', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Coffes', // основное название для типа записи
            'singular_name'      => 'Coffe', // название для одной записи этого типа
            'add_new'            => 'Добавить кофе', // для добавления новой записи
            'add_new_item'       => 'Добавление кофе', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование кофе', // для редактирования типа записи
            'new_item'           => 'Новое кофе', // текст новой записи
            'view_item'          => 'Смотреть кофе', // для просмотра записи этого типа.
            'search_items'       => 'Искать кофе', // для поиска по этим типам записи
            'not_found'          => 'Кофе не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Кофе не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Меню кофе', // название меню
        ),
        'description'         => 'Записи для кофе',
        'public'              => true,
//        'publicly_queryable'  => null,
        'exclude_from_search' => false,
        'show_ui'             => true,
//        'show_in_menu'        => null, // показывать ли в меню адмнки
//        'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
        'show_in_nav_menus'   => true,
//        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
//        'rest_base'           => null, // $post_type. C WP 4.7
//        'menu_position'       => null,
//        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'hierarchical'        => false,
        'supports'            => array('title','editor','thumbnail','excerpt'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => array('recipes'),
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ) );
}

function create_taxonomy(){

    $labels = array(
        'name'              => 'Recipes',
        'singular_name'     => 'Recipe',
        'search_items'      => 'Search Recipes',
        'all_items'         => 'All Recipes',
        'parent_item'       => 'Parent Recipe',
        'parent_item_colon' => 'Parent Recipe:',
        'edit_item'         => 'Edit Recipe',
        'update_item'       => 'Update Recipe',
        'add_new_item'      => 'Add New Recipe',
        'new_item_name'     => 'New Recipe Name',
        'menu_name'         => 'Recipe',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => 'Таксономия для рецептов', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'show_in_rest'          => null, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('recipes', array('post_coffe'), $args );
}




