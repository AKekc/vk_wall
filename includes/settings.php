<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 16.02.2017
 * Time: 11:41
 */

/**
 * Создаем страницу настроек плагина
 */

add_action('admin_menu', 'add_plugin_page');
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method(){
    wp_enqueue_script( 'jquery' );
}
function add_plugin_page(){
    $vkw_name = __( 'VK wall content', VKW_PlUGIN_TEXTDOMAIN );
  add_menu_page(  __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' . $vkw_name,  __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' .$vkw_name, 'edit_themes','vkw-setting', 'ugl_setting_page', 'https://cdn1.iconfinder.com/data/icons/iconza-circle-social/64/697032-vkontakte-32.png' );

//    add_options_page(  __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' . $vkw_name,  __('Settings', VKW_PlUGIN_TEXTDOMAIN) . ' - ' .$vkw_name, 'edit_themes', 'vkw_setting', 'ugl_setting_page' );
}
function ugl_setting_page(){
    ?>
    <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>

        <form id='logform' method="post">
            <?php
            settings_fields( 'option_group' );     // скрытые защитные поля
            do_settings_sections( 'vkw_setting_page' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
            ?>
            <div class="submit">
                <a href="#" name="save" id="gr_id_sv" class="button-primary" value="" >
                    <?php echo __('Save settings', VKW_PlUGIN_TEXTDOMAIN); ?>
                </a>
            </div>
        </form>
    </div>
    <?php
}

/**
 * Регистрируем настройки.
 * Настройки будут храниться в массиве, а не одна настройка = одна опция.
 */
add_action('admin_init', 'plugin_settings');
function plugin_settings()
{
    // параметры: $option_group, $vkw_option, $sanitize_callback
    register_setting('option_group', 'vkw_option', 'sanitize_callback');

    // параметры: $id, $title, $callback, $page
    add_settings_section('section_id', 'Основные настройки', '', 'vkw_setting_page');

    // параметры: $id, $title, $callback, $page, $section, $args
    add_settings_field('id_field', 'Введите Id группы', 'fill_id_field', 'vkw_setting_page', 'section_id');
}

## Заполняем опцию 1
function fill_id_field(){
    global $wpdb;
    $val = $wpdb->get_var( "SELECT gr_id FROM `{$wpdb->prefix}vkw_info_table` " );
    ?>
    <input type="text" class="gr_id" name="gr_id" value="<?php echo esc_attr( $val ) ?>" />


    <?php
}


## Очистка данных
function sanitize_callback( $options ){
    // очищаем
    foreach( $options as $name => & $val ){
        if( $name == 'input' )
            $val = strip_tags( $val );
    }

    //die(print_r( $options )); // Array ( [input] => aaaa [checkbox] => 1 )

    return $options;
}


add_action('admin_print_footer_scripts', 'my_action_javascript', 99);
function my_action_javascript() {

    ?>
    <script type="text/javascript" >

        jQuery('#gr_id_sv').click(function() {

            var gr_id, data;
            gr_id = jQuery('.gr_id').val();
                data = {
                action: 'my_action',
                gr_id: gr_id
            };
            console.log(gr_id);
            // с версии 2.8 'ajaxurl' всегда определен в админке
            if ( gr_id){
                jQuery.post( ajaxurl, data, function(response) {

                });
                alert('Идентификатор успешно обновлен ');


            }
            else {
                alert('Вы не ввели идентификатор');
            }

            });



    </script>
    <?php
}
add_action('wp_ajax_my_action', 'my_action_callback');
function my_action_callback() {
    global $wpdb;
    $wpdb->update( 'ugl_wp_vkw_info_table',
        array('gr_id' => $_POST['gr_id'] ),
        array( 'ID' => 1 )
    );

       wp_die(); // выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
}
        ?>

<style>
    #gr_id_sv{
        padding: 7px 16px 8px;
        margin: 0;
        font-size: 12.5px;
        display: inline-block;
        zoom: 1;
        cursor: pointer;
        white-space: nowrap;
        outline: none;
        font-family: -apple-system,BlinkMacSystemFont,Roboto,Open Sans,Helvetica Neue,sans-serif;
        vertical-align: top;
        line-height: 15px;
        text-align: center;
        text-decoration: none;
        background: none;
        background-color: #5e81a8;
        color: #fff;
        border: 0;
        border-radius: 2px;
        box-sizing: border-box;
    }
    #gr_id_sv:hover{
        background-color: #6888ad;
        text-decoration: none;

    }
</style>



