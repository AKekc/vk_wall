<?php

/*
Plugin Name: Vk Wall

Description: A brief description of the Plugin.
Version: 1.0
Author: Kekc
Author URI: https://www.facebook.com/andrey.kekc
License: A "Slug" license name e.g. GPL2
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once 'config-path.php';
//include_once VKW_PlUGIN_DIR . "includes/autoload/autoload.php";
include_once VKW_PlUGIN_DIR . "includes/function.php";
require_once VKW_PlUGIN_DIR . "includes/settings.php";


$func = new functions;

$widg = new VK_Widget;

if( ! is_admin() )
    add_filter('widget_text', 'do_shortcode');
add_action('plugins_loaded', function() {

    load_plugin_textdomain( 'vk_wall', FALSE, VKW_PlUGIN_DIR_LOCALIZATION );

} );

if ( function_exists( 'register_uninstall_hook' ) )
    register_uninstall_hook( __FILE__, array( 'functions', 'uninstall' ) );

if ( function_exists( 'register_activation_hook' ) );
register_activation_hook( __FILE__, array( 'functions', 'activation' ) );

// test


